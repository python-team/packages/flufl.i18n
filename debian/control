Source: flufl.i18n
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Pierre-Elliott Bécue <peb@debian.org>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-sequence-python3,
               pybuild-plugin-pyproject,
               python3-all,
               python3-public,
               furo <!nodoc>,
               python3-doc <!nodoc>,
               python3-hatchling,
               python3-pytest <!nocheck>,
               python3-pytest-cov <!nocheck>,
               python3-sphinx <!nodoc>,
               python3-sybil <!nocheck>
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/python-team/packages/flufl.i18n
Vcs-Git: https://salsa.debian.org/python-team/packages/flufl.i18n.git
Homepage: https://gitlab.com/warsaw/flufl.i18n
Rules-Requires-Root: no
Testsuite: autopkgtest-pkg-pybuild

Package: python-flufl.i18n-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         ${sphinxdoc:Depends}
Recommends: python3-doc
Conflicts: python-flufl.i18n-docs
Description: high level API for Python internationalization (common documentation)
 This package provides a high level, convenient API for managing
 internationalization translation contexts in Python application.  There is a
 simple API for single-context applications, such as command line scripts which
 only need to translate into one language during the entire course of their
 execution.  There is a more flexible, but still convenient API for
 multi-context applications, such as servers, which may need to switch language
 contexts for different tasks.
 .
 This is the common documentation package.

Package: python3-flufl.i18n
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Suggests: python-flufl.i18n-doc
Description: high level API for Python internationalization (Python 3)
 This package provides a high level, convenient API for managing
 internationalization translation contexts in Python application.  There is a
 simple API for single-context applications, such as command line scripts which
 only need to translate into one language during the entire course of their
 execution.  There is a more flexible, but still convenient API for
 multi-context applications, such as servers, which may need to switch language
 contexts for different tasks.
