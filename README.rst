==========
flufl.i18n
==========

A high level API for internationalizing Python libraries and applications.

The ``flufl.i18n`` library provides a convenient API for managing translation
contexts in Python applications.  It provides facilities not only for
single-context applications like command line scripts, but also more
sophisticated management of multiple-context applications such as Internet
servers.


Author
======

``flufl.i18n`` is Copyright (C) 2004-2024 Barry Warsaw <barry@python.org>

Licensed under the terms of the Apache License Version 2.0.  See the LICENSE
file for details.


Project details
===============

 * Project home: https://gitlab.com/warsaw/flufl.i18n
 * Report bugs at: https://gitlab.com/warsaw/flufl.i18n/issues
 * Code hosting: https://gitlab.com/warsaw/flufl.i18n.git
 * Documentation: https://flufli18n.readthedocs.io/
 * PyPI: https://pypi.python.org/pypi/flufl.i18n
